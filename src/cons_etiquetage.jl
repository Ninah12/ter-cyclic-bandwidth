# TER - Cyclic bandwidth
# Heuristique pour construire un étiquetage 
# MCLEAN Alexandra, RANDRIAMBOLOLONA Ninah, M1 ORO 

include("parser.jl")

# Breadth first search 
function bfs(G::Graphe, visites, phi, etiquettes, s)
    file = [s] 
    while !isempty(file)
        s_prime = file[1] ; deleteat!(file,1)
        if !visites[s_prime] 
            visites[s_prime] = true 
            phi[s_prime] = etiquettes[1] ; deleteat!(etiquettes,1)
            for t in G.L[s_prime]
                if !visites[t] 
                    append!(file, t)
                end
            end
        end
    end
end

# Construit un étiquetage pour un graphe donné en utilisant un parcours en largeur 
function cons_etiquetage(G::Graphe,     # Instance
                         s=rand(1:G.n)) # Sommet de départ du parcours en largeur
    visites = [false for i in 1:G.n]
    phi = zeros(Int,G.n) 
    etiquettes = collect(1:G.n) 
    # Parcours en largeur 
    bfs(G,visites,phi,etiquettes,s)
    println("B = ", bandwidth(G,phi), " Bc = ", cyclic_bandwidth(G,phi))
    return phi
end

# Depth first search 
function dfs(G::Graphe, visites, phi, etiquettes, x) 
    visites[x] = true 
    for y in G.L[x]
        if !visites[y] 
            phi[y] = etiquettes[1] 
            deleteat!(etiquettes,1)
            dfs(G,visites,phi,etiquettes,y)
        end
    end
end

# Construit un étiquetage pour un graphe donné en utilisant un parcours en profondeur
function cons_etiquetage_profondeur(G::Graphe,     # Instance
                                    s=rand(1:G.n)) # Sommet de départ du parcours en profondeur
    visites = [false for i in 1:G.n]
    phi = zeros(Int,G.n) 
    etiquettes = collect(2:G.n) 
    # Sommet de départ 
    phi[s] = 1 
    # Parcours en profondeur 
    dfs(G,visites,phi,etiquettes,s)
    println("Bc = ", cyclic_bandwidth(G,phi), " B = ", bandwidth(G,phi))
    return phi
end


# TER - Cyclic bandwidth
# Algorithme 1 et fonctions auxiliaires
# MCLEAN Alexandra, RANDRIAMBOLOLONA Ninah, M1 ORO 

include("parser.jl")

# Fonction alpha 
function alpha(G::Graphe, phi, u, v)
    Bc = cyclic_bandwidth(G,phi)
    if abs(phi[u] - phi[v]) <= Bc 
        return 0 
    elseif phi[u] - phi[v] >= G.n - Bc 
        return 1
    elseif phi[u] - phi[v] <= Bc - G.n 
        return -1
    end
end

### Version avec le parcours en profondeur ###
# Fonction récursive de parcours en profondeur 
function dfs(G::Graphe,  # Instance 
             phi,        # Etiquetage
             visites,    # Tableau de booléens indiquant quels sommets ont déjà été visités
             phi_etoile, # Nouvel étiquetage
             beta,       # Tableau de valeurs de la fonction beta 
             x::Int64)   # Sommet à partir duquel on fait le parcours en profondeur
    visites[x] = true 
    for y in G.L[x]
        if !visites[y] 
            beta[y] = beta[x] + alpha(G,phi,x,y)
            phi_etoile[y] = phi[y] + G.n*beta[y]
            dfs(G, phi, visites, phi_etoile, beta, y)
        end
    end
end

# Algorithme 1
function algorithme_1(G::Graphe,     # Instance
                      phi,           # Etiquetage
                      s=rand(1:G.n)) # Sommet de départ du parcours en profondeur
    visites = [false for i in 1:G.n]
    phi_etoile = zeros(Int, G.n) 
    beta = zeros(Int, G.n) 
    # Sommet de départ
    phi_etoile[s] = phi[s] 
    dfs(G, phi, visites, phi_etoile, beta, s)
    # Création de phi' 
    phi_prime = zeros(Int, G.n)
    perm = sortperm(phi_etoile)
    cpt = 1
    for i in perm 
        phi_prime[i] = cpt 
        cpt += 1
    end
    println("Bc = ", cyclic_bandwidth(G,phi_prime), " B = ", bandwidth(G,phi_prime))
    return phi_prime
end

### Version avec le parcours en largeur ###
# Parcours en largeur 
function bfs(G::Graphe, phi, visites, phi_etoile, beta, s)
    file = [s] 
    while !isempty(file)
        x = file[1] ; deleteat!(file,1)
        if !visites[x] 
            visites[x] = true 
            for y in G.L[x]
                if !visites[y] 
                    append!(file, y)
                    beta[y] = beta[x] + alpha(G,phi,x,y)
                    phi_etoile[y] = phi[y] + G.n*beta[y]
                end
            end
        end
    end
end

# Algorithme 1 avec un parcours en largeur 
function algorithme_1_PL(G::Graphe,     # Instance
                         phi,           # Etiquetage
                         s=rand(1:G.n)) # Sommet de départ du parcours en profondeur
    visites = [false for i in 1:G.n]
    phi_etoile = zeros(Int, G.n) 
    beta = zeros(Int, G.n) 
    # Sommet de départ
    phi_etoile[s] = phi[s] 
    bfs(G, phi, visites, phi_etoile, beta, s)
    # Création de phi' 
    phi_prime = zeros(Int, G.n)
    perm = sortperm(phi_etoile)
    cpt = 1
    for i in perm 
        phi_prime[i] = cpt 
        cpt += 1
    end
    println("Bc = ", cyclic_bandwidth(G,phi_prime), " B = ", bandwidth(G,phi_prime))
    return phi_prime
end
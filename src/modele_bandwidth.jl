# TER - Cyclic bandwidth
# Modèle bandwidth
# MCLEAN Alexandra, RANDRIAMBOLOLONA Ninah, M1 ORO 

include("parser.jl")

using JuMP, GLPK

# Modèle pour la bandwidth
function modele_bandwidth(solverSelected::DataType, # Solveur
                          G::Graphe, # Instance
                          b) # Valeur de la bandwidth

        m = Model(with_optimizer(solverSelected))

        # Variables
        # xij = 1 si le sommet i a l'étiquette j, 0 sinon 
        @variable(m, x[1:G.n, 1:G.n], Bin)
        # Etiquettes
        @expression(m, phi[i=1:G.n], sum( x[i,k]*k for k in 1:G.n ))

        # Valeur à minimiser 
        @expression(m, B, b)
        # Fonction objectif 
        @objective(m, Min, B)

        # Contraintes
        @constraint(m, sommet[i=1:G.n], sum(x[i,j] for j in 1:G.n)==1) # Chaque sommet a une seule étiquette
        @constraint(m, etiquette[j=1:G.n], sum(x[i,j] for i in 1:G.n)==1) # Chaque étiquette est affectée à un seul sommet 
        # Minimisation de la bandwidth 
        @constraint(m, Bandwidth[i=1:G.n, j=1:length(G.L[i])], -B <= phi[i] - phi[G.L[i][j]] <= B)

        # Résolution
        JuMP.optimize!(m)

        # Affichage
        status = termination_status(m)

        if status == MOI.OPTIMAL
            println("Problème résolu à l'optimalité")
            println("B = ", objective_value(m))
            for i in 1:G.n 
                println("$i : ", sum(value.(x[i,k])*k for k in 1:G.n))
            end
        elseif status == MOI.INFEASIBLE
            println("Problème impossible")
        elseif status == MOI.INFEASIBLE_OR_UNBOUNDED
            println("Problème non borné")
        else 
            println(status)
        end

end

    

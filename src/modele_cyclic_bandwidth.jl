# TER - Cyclic bandwidth
# Modele pour la cyclic bandwidth
# MCLEAN Alexandra, RANDRIAMBOLOLONA Ninah, M1 ORO 

include("parser.jl")

using JuMP, GLPK

# Modèle pour la cyclic bandwidth
function modele_cyclic_bandwidth(solverSelected::DataType, # Solveur
                                 G::Graphe, # Instance
                                 cb) # Valeur de la cyclic bandwidth

        m = Model(with_optimizer(solverSelected))

        # Variables
        # xij = 1 si le sommet i a l'étiquette j, 0 sinon 
        @variable(m, x[1:G.n, 1:G.n], Bin)
        # cij = valeur de la cyclic bandwidth sur l'arête (i,j)
        @variable(m, c[1:G.n, 1:G.n], Int)
        @variable(m, b[1:G.n, 1:G.n], Bin)
        @variable(m, y[1:G.n, 1:G.n], Bin)
        @variable(m, t[1:G.n, 1:G.n], Bin)

        # Etiquettes
        @expression(m, phi[i=1:G.n], sum(x[i,k]*k for k in 1:G.n))

        # Valeur à minimiser 
        @expression(m, M, 2*G.n)
        @expression(m, M1, 2*G.n)
        @expression(m, M2, 2*G.n)
        @expression(m, M3, 2*G.n)
        @expression(m, CB, cb)
        # Fonction objectif 
        @objective(m, Min, CB)

        # Contraintes
        @constraint(m, sommet[i=1:G.n], sum(x[i,j] for j in 1:G.n)==1) # Chaque sommet a une seule étiquette
        @constraint(m, etiquette[j=1:G.n], sum(x[i,j] for i in 1:G.n)==1) # Chaque étiquette est affectée à un seul sommet 

        # Fixe l'étiquette du premier sommet à 1
        @constraint(m, x[1,1] == 1)
        
        # C1
        @constraint(m, A[i=1:G.n, j=1:length(G.L[i])], phi[i] - phi[G.L[i][j]] >= c[i,G.L[i][j]] - M1*y[i,G.L[i][j]])
        @constraint(m, B[i=1:G.n, j=1:length(G.L[i])], phi[i] - phi[G.L[i][j]] <= -c[i,G.L[i][j]] + M1*(1-y[i,G.L[i][j]]))
        # C2
        @constraint(m, C[i=1:G.n, j=1:length(G.L[i])], phi[i] - phi[G.L[i][j]] <= G.n - c[i,G.L[i][j]])
        @constraint(m, D[i=1:G.n, j=1:length(G.L[i])], phi[G.L[i][j]] - phi[i] <= G.n - c[i,G.L[i][j]])
        # C3
        @constraint(m, E[i=1:G.n, j=1:length(G.L[i])], phi[i] - phi[G.L[i][j]] <= c[i,G.L[i][j]] + M2*b[i,G.L[i][j]])
        @constraint(m, F[i=1:G.n, j=1:length(G.L[i])], phi[G.L[i][j]] - phi[i] <= c[i,G.L[i][j]] + M2*b[i,G.L[i][j]])
        # C4
        @constraint(m, H[i=1:G.n, j=1:length(G.L[i])], phi[i] - phi[G.L[i][j]] >= G.n - M*(1-b[i,G.L[i][j]]) - c[i,G.L[i][j]] - M3*t[i,G.L[i][j]])
        @constraint(m, I[i=1:G.n, j=1:length(G.L[i])], phi[i] - phi[G.L[i][j]] <= M*(1-b[i,G.L[i][j]]) + c[i,G.L[i][j]] - G.n + M3*(1-t[i,G.L[i][j]]))

        @constraint(m, J[i=1:G.n, j=1:length(G.L[i])], c[i,G.L[i][j]] <= CB)

        # Résolution
        JuMP.optimize!(m)

        # Affichage
        status = termination_status(m)

        if status == MOI.OPTIMAL
            println("Problème résolu à l'optimalité")
            println("Bc = ", objective_value(m))
            for i in 1:G.n 
                println("$i : ", sum(value.(x[i,k])*k for k in 1:G.n))
            end
        elseif status == MOI.INFEASIBLE
            println("Problème impossible")
        elseif status == MOI.INFEASIBLE_OR_UNBOUNDED
            println("Problème non borné")
        else 
            println(status)
        end

end

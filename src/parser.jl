# TER - Cyclic bandwidth
# Parseur, structures de données et fonctions de base 
# MCLEAN Alexandra, RANDRIAMBOLOLONA Ninah, M1 ORO 

struct Graphe 
    nom::String # nom de l'instance 
    n::Int      # nombre de sommets
    m::Int      # nombre d'arêtes
    L # liste d'adjacence représentant le Graphe
end

# Parse une instance
function parser(fname)
    f = open(fname)
    # Lecture du nom 
    B = split(readline(f))
    if length(B) > 1 
        nom = B[4]
    else
        nom = B[1]
    end
    # Lecture du nombre de sommets (n) et le nombre d'arêtes (m)
    A = split(readline(f))
    n = parse(Int, A[1])
    m = parse(Int, A[3])
    # Construction de la liste d'adjacence
    L =[Int64[] for i in 1:n]
    for i in 1:m
        u, v = parse.(Int, split(readline(f)))
        push!(L[u],v)
        push!(L[v],u)
    end
    return Graphe(nom,n,m,L)
end 

# Calcule la cyclic bandwidth 
function cyclic_bandwidth(G::Graphe, phi)
    Bc = 0 # cyclic bandwidth 
    # On parcourt toutes les arêtes 
    for i in 1:length(G.L) 
        for j in 1:length(G.L[i])
            cb = min(abs(phi[i] - phi[G.L[i][j]]), G.n - abs(phi[i] - phi[G.L[i][j]]))
            Bc = max(Bc, cb)
        end
    end
    return Bc 
end 

# Calcule la bandwidth 
function bandwidth(G::Graphe, phi)
    B = 0 # bandwidth 
    # On parcourt toutes les arêtes 
    for i in 1:length(G.L) 
        for j in 1:length(G.L[i])
            b = abs(phi[i] - phi[G.L[i][j]])
            B = max(B, b)
        end
    end
    return B 
end 

# Parseurs pour les étiquetages NILS et Tabou 
function parseur_chaine(chaine)
    phi = Int64[]
    etiquettes = split(chaine)
    for e in etiquettes
        append!(phi, parse(Int64,e))
    end
    return phi 
end

function parseur_etiquetages(fichier)
    liste_etiquetages = []
    f = open(fichier) 
    line = readline(f)
    while(!isempty(line))
        phi = parseur_chaine(line)
        push!(liste_etiquetages, phi)
        readline(f)
        line = readline(f)
    end
    return liste_etiquetages
end
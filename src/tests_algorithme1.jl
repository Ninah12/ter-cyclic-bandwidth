# TER - Cyclic bandwidth 
# Tests de l'algorithme 1 en utilisant des étiquetages fournis
# MCLEAN Alexandra, RANDRIAMBOLOLONA Ninah, M1 ORO 

include("algorithme_1.jl")

# Affiche les valeurs de Bc et B 
function test_CB_B(G,phi) 
    println(G.nom)
    println("Bc = ", cyclic_bandwidth(G,phi), " B = ", bandwidth(G,phi))
end

# Tester l'algorithme 1 sur un graphe et un étiquetage donnés avec le parcours en profondeur
# et en largeur, en testant tous les sommets de départ
function test_PP_PL_tsd(G, phi)
    println(G.nom)
    # Teste tous les sommets de départ
    for s in 1:G.n 
        println("\nSommet de départ $s")
        phi_PP = algorithme_1(G,phi,s)
        println("Parcours en profondeur : phi' = ", phi_PP)
        phi_PL = algorithme_1_PL(G,phi,s)
        println("Parcours en largeur : phi' = ", phi_PL)
    end
end

# Passe un étiquetage à travers l'algorithme 1 (parcours en profondeur) jusqu'à ce qu'il n'y 
# ait plus d'amélioration de la valeur de la cyclic bandwidth ou de la bandwidth
function test_amelioration(G,        # Graphe 
                           phi_init) # Etiquetage initial
    println(G.nom)
    println("Etiquetage initial : phi_init = ", phi_init) 
    for i in 1:G.n 
        phi = copy(phi_init)
        println("\nSommet de départ $i")
        Bc = cyclic_bandwidth(G,phi_init) 
        B = bandwidth(G,phi_init) 
        est_amelioree = true 
        iter = 1 
        while est_amelioree 
            println("\nIteration $iter")
            phi_prime = algorithme_1(G,phi,i)
            println("phi_prime = ", phi_prime)
            Bc_prime = cyclic_bandwidth(G,phi_prime) 
            B_prime = bandwidth(G,phi_prime)
            est_amelioree = (Bc_prime < Bc || B_prime < B)
            iter = iter + 1
            Bc = Bc_prime ; B = B_prime ; phi = phi_prime 
        end
    end
end

# Passe un étiquetage à travers l'algorithme 1 (parcours en largeur) jusqu'à ce qu'il n'y 
# ait plus d'amélioration de la valeur de la cyclic bandwidth ou de la bandwidth
function test_amelioration_PL(G,        # Graphe 
                              phi_init) # Etiquetage initial
    println(G.nom)
    println("Etiquetage initial : phi_init = ", phi_init) 
    for i in 1:G.n 
        phi = copy(phi_init)
        println("\nSommet de départ $i")
        Bc = cyclic_bandwidth(G,phi_init) 
        B = bandwidth(G,phi_init) 
        est_amelioree = true 
        iter = 1 
        while est_amelioree 
            println("\nIteration $iter")
            phi_prime = algorithme_1_PL(G,phi,i)
            println("phi_prime = ", phi_prime)
            Bc_prime = cyclic_bandwidth(G,phi_prime) 
            B_prime = bandwidth(G,phi_prime)
            est_amelioree = (Bc_prime < Bc || B_prime < B)
            iter = iter + 1
            Bc = Bc_prime ; B = B_prime ; phi = phi_prime 
        end
    end
end


# Algorithme de Horton

import sys
import itertools as it
import networkx as nx
from itertools import combinations
import time

#######################################################################################
### MAIN ##########################################################################################
### MAIN ##########################################################################################
### MAIN ##########################################################################################
### MAIN ###

if __name__ == '__main__':

    # Chemin du graphe
    path = input("Graphe : ")
    
    mm = 0
    mygraph = []
    with open(path, "r") as inputf:
        for line in inputf: # read rest of lines
            if mm==0:
                s = line.split(' ')
                print('=======================')
                print('{}======================='.format(s[3]))
                
            mm = mm +1
            if mm>=3:
                mygraph.append([int(x) for x in line.split()])

    G = nx.Graph()
    G.add_edges_from(mygraph)

    n = G.number_of_nodes()
 
    print('n = {}'.format(G.number_of_nodes()))
    print('number of edges m = {}\n'.format(G.number_of_edges()))
    #print('clique number = {}'.format(nx.graph_clique_number(G)))

    c = nx.minimum_cycle_basis(G)

    print('Minimum Weight Cycle Basis : {}\n'.format(c))

    longest_cycle = max(c, key=len)

    print('Longest cycle in cycle basis = {}\n'.format(longest_cycle))

    l = len(longest_cycle)

    print('===============')
    print('==> l = {}'.format(l))
    print('===============')
    
    ##print([sorted(c) for c in nx.minimum_cycle_basis(G)])
